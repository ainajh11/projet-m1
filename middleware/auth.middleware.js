const UserModel = require("../models/user.model");
const jwt = require("jsonwebtoken");

module.exports.checkUser = (req, res, next) => {
  const token = req.headers.authorization;
  if (token) {
    jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
      if (err) {
        res.locals.user = null;
        res.cookie("mon_token", "", { maxAge: 1 });
        next();
      } else {
        let user = await UserModel.findById(decodedToken.id);
        res.locals.user = user;
        console.log(user);
        next();
      }
    });
  } else {
    res.locals.user = null;
    next();
  }
};
module.exports.authorisation = (req, res, next) => {
  const token = req.headers.authorization;
  if (token) {
    jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
      if (err) {
        res.locals.user = null;
        res.status(401).json({ message: "not Authorized" });
        next();
      } else {
        let user = await UserModel.findById(decodedToken.id);

        if (user.role == "admin") {
          console.log("you are admin");
          res.locals.user = user;
          next();
        } else if (user.role != "admin") {
          console.log("You are not Authorized");
          res.status(400).json({ message: "You are not Authorized" });
        }
      }
    });
  } else {
    res.locals.user = null;
    res.status(400).json({ message: "You are not connected" });
  }
};

module.exports.requireAuth = (req, res, next) => {
  const token = req.headers.authorization; //ito le cookie 'jwt'
  if (token) {
    jwt.verify(token, process.env.TOKEN_SECRET, async (err, decodedToken) => {
      if (err) {
        console.log(err);
      } else {
        console.log(decodedToken.id);
        next();
      }
    });
  } else {
    console.log("no token");
  }
};
