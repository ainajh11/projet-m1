const mongoose = require('mongoose')

mongoose
  .connect(
    "mongodb://127.0.0.1:27017/projet",
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    }
  )
  .then(() => {
    console.log("connected to mongoDb");
  })
  .catch((err) => {
    console.log("failed to connect to MongoDb", err);
  });