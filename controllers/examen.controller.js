const ExamenModel = require('../models/examen.model');
const MatiereModel = require('../models/matiere.model');
const UserModel = require('../models/user.model');
const ObjectId = require('mongoose').Types.ObjectId;

module.exports.addNoteExamen = async (req, res) => {
  const { note, matiere, etudiant, semestre } = req.body;

  if (!ObjectId.isValid(matiere))
    return res.status(400).send({ error: `matiere inconnu` });
  if (!ObjectId.isValid(etudiant))
    return res.status(400).send({ error: `etudiant inconnu` });

  const newExamen = new ExamenModel({
    note,
    matiere,
    etudiant,
    semestre,
  });

  try {
    const examen = await newExamen.save();
    res.status(201).json({ examen: `success` });
  } catch (err) {
    res.status(200).send({ err });
  }
};

module.exports.getAllExamen = async (req, res) => {
  const results = await Promise.all([
    ExamenModel.aggregate([
      {
        $lookup: {
          from: MatiereModel.collection.name,
          localField: 'matiere',
          foreignField: '_id',
          as: 'matiere',
        },
      },
      {
        $lookup: {
          from: UserModel.collection.name,
          localField: 'etudiant',
          foreignField: '_id',
          as: 'etudiant',
        },
      },
    ]).exec(),
  ]).catch((err) => {
    res.status(500).send({
      message: err.message || 'Some error occurred while retrieving entry.',
    });
  });

  if (req.accepts('json')) {
    res.status(200).json({ results });
  }
};

module.exports.getExamenById = (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send({ error: `ID ${req.params.id} matiere inconnu` });
  ExamenModel.findById(req.params.id, (err, docs) => {
    if (!err) res.send(docs);
    else res.send('examen inconnu ' + err);
  }).select(['-createdAt', '-updatedAt']);
};

module.exports.updateExamen = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send({ error: `ID ${req.params.id} examen inconnu` });

  const { note, matiere, etudiant, semestre } = req.body;
  console.log(req.body);

  if (!note || !semestre)
    return res
      .status(400)
      .send({ err: 'Les données à updater ne peuvent pas etre vide' });

  try {
    await ExamenModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          note,
          matiere,
          etudiant,
          semestre,
        },
      },
      { new: true },
      (err, docs) => {
        if (!ObjectId.isValid(matiere))
          return res.status(400).send({ error: `matiere inconnu` });

        if (!ObjectId.isValid(etudiant))
          return res.status(400).send({ error: `etudiant inconnu` });

        if (!err) return res.send(docs);

        if (err) return res.status(500).send({ message: err });
      }
    ).select(['-createdAt', '-updatedAt']);
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.deleteExamen = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res.status(400).send({ error: `Examen inconnu` });

  try {
    await ExamenModel.remove({ _id: req.params.id }).exec();
    res.status(200).json({ message: ` Suppression avec succès` });
  } catch (err) {
    return res.status(400).json({ message: err });
  }
};

module.exports.getByEtudiant = async (req, res) => {
  console.log(req.params);
  try {
    const notes = await ExamenModel.find({
      etudiant: req.params.id,
    })
      .populate({ path: 'matiere' })
      .populate({ path: 'etudiant' });
    if (notes) {
      res.send(notes);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    console.log('error ', error);
    return res.status(500).json(error);
  }
};
