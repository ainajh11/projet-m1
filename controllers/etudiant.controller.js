const EtudiantModel = require('../models/etudiant.model');
const ObjectId = require('mongoose').Types.ObjectId;
const { etudiantErrors } = require('../utils/errors.utils');
const maxAge = 3 * 24 * 60 * 60 * 1000;
module.exports.createEtudiant = async (req, res) => {
  const { nom, numero, parcours, niveau, dateNaiss } = req.body;
  try {
    const etudiant = await EtudiantModel.create({
      nom,
      numero,
      parcours,
      niveau,
      dateNaiss,
    });
    res
      .status(201)
      .json({ data: `creation d'etudiant avec success`, success: true });
  } catch (err) {
    // res.status(200).send({err})
    const errors = etudiantErrors(err);
    res.status(200).send({ errors: errors, success: false });
  }
};

module.exports.getAllEtudiant = async (req, res) => {
  const etudiant = await EtudiantModel.find()
    .sort({ numero: 1 })
    .select(['-createdAt', '-updatedAt']);
  res.status(200).json({ etudiant });
};

module.exports.getEtudiantByID = (req, res) => {
  console.log(req.params);
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send({ error: `ID ${req.params.id} etudiant inconnu` });
  EtudiantModel.findById(req.params.id, (err, docs) => {
    if (!err) res.send(docs);
    else res.send('ID etudiant inconnu ' + err);
  }).select(['-createdAt', '-updatedAt']);
};

module.exports.getEtudiantByNiveau = (req, res) => {
  EtudiantModel.find(
    {
      $and: [
        {
          niveau: {
            $regex: new RegExp('^' + req.params.niveau.toLowerCase(), 'i'),
          },
        },
        {
          parcours: {
            $regex: new RegExp('^' + req.params.parcours.toLowerCase(), 'i'),
          },
        },
      ],
    },
    (err, docs) => {
      if (!err) res.send(docs);
      else res.send(err);
    }
  )
    .sort({ numero: 1 })
    .select(['-createdAt', '-updatedAt']);
};

module.exports.updateEtudiant = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send({ error: `ID ${req.params.id} etudiant inconnu` });
  try {
    await EtudiantModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          nom: req.body.nom,
          numero: req.body.numero,
          parcours: req.body.parcours,
          niveau: req.body.niveau,
          dateNaiss: req.body.dateNaiss,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (err, docs) => {
        if (!err) return res.send({ docs, success: true });
        if (err) return res.status(500).send({ message: err });
      }
    ).select(['-createdAt', '-updatedAt']);
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.deleteEtudiant = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send({ error: `ID ${req.params.id} etudiant inconnu` });

  try {
    await EtudiantModel.remove({ _id: req.params.id }).exec();
    res
      .status(200)
      .json({ message: `L'ID ${req.params.id} est supprimé avec succès` });
  } catch (err) {
    return res.status(400).json({ message: err });
  }
};
