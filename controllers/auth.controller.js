const EtudiantModel = require('../models/etudiant.model');
const jwt = require('jsonwebtoken');
const { registerErrors } = require('../utils/errors.utils');
const UserModel = require('../models/user.model');

const maxAge = 1 * 24 * 60 * 60 * 1000;
const createToken = (id) => {
  return jwt.sign({ id }, process.env.TOKEN_SECRET, {
    expiresIn: maxAge,
  });
};

module.exports.signUp = async (req, res) => {
  const {
    pseudo,
    password,
    role,
    nom,
    numero,
    parcours,
    niveau,
    dateNaiss,
    matricule,
  } = req.body;
  console.log(req.body);
  try {
    const user = await UserModel.create({
      pseudo,
      password,
      role,
      nom,
      numero,
      parcours,
      niveau,
      dateNaiss,
      matricule,
    });
    res
      .status(201)
      .json({ user: user._id, message: "Creation d'utilisateur avec success" });
  } catch (err) {
    const errors = registerErrors(err);
    res.status(400).send(errors);
    // res.status(400).send(err.errors.pseudo );
  }
};
module.exports.signIn = async (req, res) => {
  const { pseudo, password } = req.body;

  try {
    const user = await UserModel.login(pseudo, password);
    const token = createToken(user._id);
    res.cookie('mon_token', token, { httpOnly: true, maxAge });
    res.status(200).json({ user: user, token: token, success: true });
  } catch (err) {
    res.status(401).json({
      message: "Creer d'abord votre compte personnel",
      success: false,
    });
  }
};
module.exports.logOut = (req, res) => {
  res.cookie('mon_token', '', { maxAge: 1 });
  //  res.redirect('http://localhost:5000/api/user/okForYou');

  res.status(200).json({ message: 'Deconnécté! à plus tard' });
};

module.exports.okForYou = (req, res) => {
  res.status(200).json({ message: 'Deconnécté! à plus tard' });
};
