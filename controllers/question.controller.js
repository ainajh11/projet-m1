const QuestionModel = require('../models/question.model');
const MatiereModel = require('../models/matiere.model');
const ObjectId = require('mongoose').Types.ObjectId;

module.exports.createQuestion = async (req, res) => {
  const {
    question,
    time,
    all_reponse,
    true_reponse,
    identifiant,
    matiere,
    semestre,
  } = req.body;

  if (!ObjectId.isValid(req.body.matiere))
    return res
      .status(400)
      .send({ error: `ID ${req.body.matiere} matiere inconnu` });

  const newQuestion = new QuestionModel({
    question,
    time,
    all_reponse,
    true_reponse,
    identifiant,
    matiere,
    semestre,
  });

  //   if (!all_reponse.includes(true_reponse))
  //     return res.status(400).send({
  //       error: "La vraie réponse doit apparaitre dans les reponses suggérées",
  //     });

  try {
    const questions = await newQuestion.save();
    res
      .status(201)
      .json({ matiere: `creation de question avec success`, success: true });
  } catch (err) {
    res.status(200).send({ err, success: false });
  }
};

module.exports.getAllQuestions = async (req, res) => {
  const results = await Promise.all([
    QuestionModel.aggregate([
      {
        $lookup: {
          from: MatiereModel.collection.name,
          localField: 'matiere',
          foreignField: '_id',
          as: 'matiere',
        },
      },
    ]).exec(),
    QuestionModel.countDocuments({}),
  ]).catch((err) => {
    res.status(500).send({
      message: err.message || 'Some error occurred while retrieving entry.',
    });
  });

  if (req.accepts('json')) {
    res.send(results);
  } else {
    res.json('entry', {
      questions: results,
    });
  }
};
module.exports.getQuestionByIdentifiant = async (req, res) => {
  const results = await Promise.all([
    QuestionModel.aggregate([
      {
        $lookup: {
          from: MatiereModel.collection.name,
          localField: 'matiere',
          foreignField: '_id',
          as: 'matiere',
        },
      },
    ])
      .match({
        identifiant: req.params.identifiant,
      })
      .exec(),
    QuestionModel.countDocuments({}),
  ]).catch((err) => {
    res.status(500).send({
      message: err.message || 'Some error occurred while retrieving entry.',
    });
  });

  if (req.accepts('json')) {
    res.send(results);
  } else {
    res.json('entry', {
      questions: results,
    });
  }
};
module.exports.getQuestionByID = async (req, res) => {
  const results = await Promise.all([
    QuestionModel.aggregate([
      {
        $lookup: {
          from: MatiereModel.collection.name,
          localField: 'matiere',
          foreignField: '_id',
          as: 'matiere',
        },
      },
    ])
      .match({
        _id: ObjectId(req.params.id),
      })
      .exec(),
    QuestionModel.countDocuments({}),
  ]).catch((err) => {
    res.status(500).send({
      message: err.message || 'Some error occurred while retrieving entry.',
    });
  });

  if (req.accepts('json')) {
    res.send(results);
  } else {
    res.json('entry', {
      questions: results,
    });
  }
};

// module.exports.getQuestionByID = (req, res) => {
//   if (!ObjectId.isValid(req.params.id))
//     return res
//       .status(400)
//       .send({ error: `ID ${req.params.id} question inconnu` });
//   QuestionModel.findById(req.params.id, (err, docs) => {
//     if (!err) res.status(200).json({ questions: docs });
//     else res.send("question inconnu " + err);
//   });
// };

module.exports.updateQuestion = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send({ error: `ID ${req.params.id} matiere inconnu` });
  try {
    await QuestionModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          question: req.body.question,
          all_reponse: req.body.all_reponse,
          true_reponse: req.body.true_reponse,
          identifiant: req.body.identifiant,
          matiere: req.body.matiere,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (err, docs) => {
        // if (!req.body.all_reponse.includes(req.body.true_reponse))
        //   return res.status(400).send({
        //     error:
        //       "La vraie réponse doit apparaitre dans les reponses suggérées",
        //   });

        if (!err) return res.send({ success: true });
        if (err) return res.status(500).send({ message: err, success: false });
      }
    );
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.deleteQuestion = async (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send({ error: `ID ${req.params.id} question inconnu` });

  try {
    await QuestionModel.remove({ _id: req.params.id }).exec();
    res.status(200).json({ message: 'Suppression avec succès' });
  } catch (err) {
    return res.status(400).json({ message: err });
  }
};
