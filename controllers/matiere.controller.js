const MatiereModel = require('../models/matiere.model')
const ObjectId = require('mongoose').Types.ObjectId
const { matiereErrors } = require("../utils/errors.utils");
module.exports.createMatiere = async (req,res) =>{
    const {libelle,coefficient,parcours,niveau} = req.body;
    try{
        const matiere = await MatiereModel.create({libelle,coefficient,parcours,niveau})
        res.status(201).json({ data: `creation de matiere avec success`, success: true });
    }
    catch(err){
        const errors = matiereErrors(err);
        res.status(200).send({ errors: errors, success: false });
    }
}

module.exports.getAllMatiere = async (req,res) => {
    const matieres = await MatiereModel.find().select(['-createdAt','-updatedAt'])
    res.status(200).json({matieres})
}

module.exports.getMatiereByID =  (req,res) =>{
    console.log(req.params);
    if(!ObjectId.isValid(req.params.id))
      return res.status(400).send( {error : `ID ${req.params.id} matiere inconnu`})
    MatiereModel.findById(req.params.id, (err,docs) => {
        if(!err) res.send(docs)
        else res.send("ID matiere inconnu " + err)
    }).select(['-createdAt','-updatedAt'])
}

module.exports.updateMatiere = async (req,res) => {
    if(!ObjectId.isValid(req.params.id))
      return res.status(400).send( {error : `ID ${req.params.id} matiere inconnu`})
    try {
        await MatiereModel.findOneAndUpdate(
            {_id: req.params.id},
            {
            $set: {
                libelle: req.body.libelle,
                coefficient : req.body.coefficient,
                parcours:req.body.parcours,
                niveau:req.body.niveau,
            }
            },
            {new: true, upsert:true,setDefaultsOnInsert: true},
            (err,docs)=>{
                if (!err) return res.send({ docs, success: true });
                if(err) return res.status(500).send({message: err})
            }
        ).select(['-createdAt','-updatedAt'])
        
    } catch (err) {
        const errors = matiereErrors(err);
        res.status(200).send({ errors: errors, success: false });
    }
}

module.exports.deleteMatiere = async (req,res) => {
    if(!ObjectId.isValid(req.params.id))
      return res.status(400).send( {error : `ID ${req.params.id} matiere inconnu`})

    try{
        await MatiereModel.remove({_id: req.params.id}).exec()
        res.status(200).json({message: `L'ID ${req.params.id} est supprimé avec succès`})
    } catch(err){
        return res.status(400).json({message: err})
    }

}

module.exports.getMatiereByNiveau = (req, res) => {   
    MatiereModel.find(
      {
          "$and":[
           { niveau : { $regex: new RegExp('^' + req.params.niveau.toLowerCase(), 'i') }},
           { parcours : { $regex: new RegExp('^' + req.params.parcours.toLowerCase(), 'i') }},
          ]
        },
      (err, docs) => {
        if (!err) res.send(docs);
        else res.send( err);
      }
    ).select(["-createdAt", "-updatedAt"]);
  };