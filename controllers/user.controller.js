const UserModel = require('../models/user.model');
const ObjectID = require('mongoose').Types.ObjectId;
const bcrypt = require('bcrypt');

module.exports.getAllUsers = async (req, res) => {
  const users = await UserModel.find().select('-password');
  res.status(201).send(users);
};

module.exports.getAllEtudiantUser = (req, res) => {
  UserModel.find({ role: 'etudiant' }, (err, docs) => {
    if (!err) res.send(docs);
    else res.send('ID etudiant inconnu ' + err);
  }).select(['-createdAt', '-updatedAt']);
};
module.exports.getAllAdminUser = (req, res) => {
  UserModel.find({ role: 'admin' }, (err, docs) => {
    if (!err) res.send(docs);
    else res.send('ID etudiant inconnu ' + err);
  }).select(['-createdAt', '-updatedAt']);
};

module.exports.userInfo = (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(404).json({ error: 'User inconnu' });

  UserModel.findById(req.params.id, (err, docs) => {
    if (!err) return res.status(200).send(docs);
    else res.status(404).json({ error: 'User Inconnu' });
  }).select('-password');
};

module.exports.updateUser = async (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(404).json({ error: 'User inconnu' });

  try {
    const salt = await bcrypt.genSalt();
    req.body.password = await bcrypt.hash(req.body.password, salt);

    await UserModel.findOneAndUpdate(
      {
        _id: req.params.id,
      },
      {
        $set: {
          pseudo: req.body.pseudo,
          password: req.body.password,
          role: req.body.role,
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (err, docs) => {
        if (!err) return res.status(200).send(docs);
        else return res.status(500).json({ message: err });
      }
    );
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};
module.exports.updateUserEtudiant = async (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(404).json({ error: 'User inconnu' });

  try {
    await UserModel.findOneAndUpdate(
      {
        _id: req.params.id,
      },
      {
        $set: {
          nom: req.body.nom,
          numero: req.body.numero,
          parcours: req.body.parcours,
          niveau: req.body.niveau,
          dateNaiss: req.body.dateNaiss,
          matricule: req.body.matricule,
        },
      },

      (err, docs) => {
        if (!err)
          return res
            .status(200)
            .send({ success: true, message: 'Modification avec success' });
        else return res.status(500).json({ message: err });
      }
    );
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.deleteUser = async (req, res) => {
  if (!ObjectID.isValid(req.params.id))
    return res.status(404).json({ error: 'User inconnu' });

  try {
    await UserModel.remove({ _id: req.params.id }).exec();
    res.status(200).json({ message: `Suppression avec succès` });
  } catch (err) {
    return res.status(500).json({ message: err });
  }
};

module.exports.getEtudiantByNiveau = (req, res) => {
  UserModel.find(
    {
      $and: [
        {
          niveau: {
            $regex: new RegExp('^' + req.params.niveau.toLowerCase(), 'i'),
          },
        },
        {
          parcours: {
            $regex: new RegExp('^' + req.params.parcours.toLowerCase(), 'i'),
          },
        },
        { role: 'etudiant' },
      ],
    },
    (err, docs) => {
      if (!err) res.send(docs);
      else res.send(err);
    }
  )
    .sort({ numero: 1 })
    .select(['-createdAt', '-updatedAt']);
};

module.exports.getByMatricule = async (req, res) => {
  console.log(req.params);
  try {
    const etudiant = await UserModel.find({
      matricule: req.params.matricule,
    });
    if (etudiant) {
      res.send(etudiant);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    console.log('error ', error);
    return res.status(500).json(error);
  }
};
