const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const matiereRoutes = require("./routes/matiere.routes");
const userRoutes = require("./routes/user.routes");
const etudiantRoutes = require("./routes/etudiant.routes");
const questionRoutes = require("./routes/question.routes");
const examenRoutes = require("./routes/examen.routes");
require("dotenv").config({ path: "./config/.env" });
require("./config/db");
const { checkUser, requireAuth } = require("./middleware/auth.middleware");
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Methods',
    'GET, POST, PATCH, PUT, DELETE, OPTIONS'
  );
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  if (req.method === 'OPTIONS') {
    return res.status(200).end();
  }
  next();
});


//jwt
app.get("*", checkUser);
app.get("/jwtid", requireAuth, (req, res) => {
  res.status(200).json({ id_user: res.locals.user._id });
});

//routes
app.use("/api/user", userRoutes);
app.use("/api/matiere", matiereRoutes);
app.use("/api/etudiant", etudiantRoutes);
app.use("/api/question", questionRoutes);
app.use("/api/examen", examenRoutes);

//server
app.listen(process.env.PORT, () => {
  console.log(`Listening on port ${process.env.PORT}`);
});
