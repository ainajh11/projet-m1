module.exports.etudiantErrors = (err) => {
  let errors = { nom: "", numero: "", parcours: "", niveau: "" };

  if (err.message.includes("nom")) errors.nom = "Nom incorrect ou déjà pris";
  if (err.message.includes("numero"))
    errors.numero = "numero incorrect ou déjà pris";
  if (err.message.includes("parcours"))
    errors.parcours = "parcours incorrect ou déjà pris";
  if (err.message.includes("niveau"))
    errors.niveau = "niveau incorrect ou déjà pris";

  return errors;
};
module.exports.matiereErrors = (err) => {
  let errors = { libelle: "", coefficient: "", parcours: "", niveau: "" };

  if (err.message.includes("libelle"))
    errors.libelle = "Nom incorrect ou déjà pris";
  if (err.message.includes("coefficient"))
    errors.coefficient = "numero incorrect ou déjà pris";
  if (err.message.includes("parcours"))
    errors.parcours = "parcours incorrect ou déjà pris";
  if (err.message.includes("niveau"))
    errors.niveau = "niveau incorrect ou déjà pris";

  return errors;
};
module.exports.registerErrors = (err) => {
  let errors = {
    pseudo: "",
    nom: "",
    error: true
  };

  if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("pseudo"))
    errors.pseudo = "Pseudo incorrect ou déjà pris";
  if (err.code === 11000 && Object.keys(err.keyValue)[0].includes("nom"))
    errors.nom = "Nom incorrect ou déjà pris";
  
  // if (err.errors.pseudo.kind.equals("minlength"))
  // errors.parcours = "parcours incorrect ou déjà pris";
  // if (err.message.includes("niveau"))
  // errors.niveau = "niveau incorrect ou déjà pris";

  return errors;
};
