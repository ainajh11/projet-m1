const router = require('express').Router();
const etudiantController = require('../controllers/etudiant.controller');
const { authorisation } = require('../middleware/auth.middleware');

router.post('/', authorisation, etudiantController.createEtudiant);
router.get('/', etudiantController.getAllEtudiant);
router.get('/:id', etudiantController.getEtudiantByID);
router.get('/search/:niveau/:parcours', etudiantController.getEtudiantByNiveau);
router.put('/:id', authorisation, etudiantController.updateEtudiant);
router.delete('/:id', authorisation, etudiantController.deleteEtudiant);

module.exports = router;
