const router = require('express').Router();
const questionController = require('../controllers/question.controller')
const {authorisation} = require('../middleware/auth.middleware')


router.post('/',authorisation , questionController.createQuestion)
router.get('/',  questionController.getAllQuestions)
router.get('/:id', questionController.getQuestionByID)
router.get('/by-identifiant/:identifiant', questionController.getQuestionByIdentifiant)
router.put('/:id',authorisation , questionController.updateQuestion)
router.delete('/:id',authorisation , questionController.deleteQuestion)

module.exports = router
