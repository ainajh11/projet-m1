const router = require('express').Router();
const matiereController = require('../controllers/matiere.controller')
const {authorisation} = require('../middleware/auth.middleware')


router.post('/',authorisation , matiereController.createMatiere)
router.get('/', matiereController.getAllMatiere)
router.get('/:id', matiereController.getMatiereByID)
router.get('/search/:niveau/:parcours', matiereController.getMatiereByNiveau)
router.put('/:id',authorisation , matiereController.updateMatiere)
router.delete('/:id',authorisation , matiereController.deleteMatiere)

module.exports = router
