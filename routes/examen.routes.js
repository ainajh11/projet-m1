const router = require('express').Router();
const examenController = require('../controllers/examen.controller');
const { authorisation } = require('../middleware/auth.middleware');

router.post('/', examenController.addNoteExamen);
router.get('/etudiant/:id', examenController.getByEtudiant);
router.get('/', examenController.getAllExamen);
router.get('/:id', examenController.getExamenById);
router.put('/:id', authorisation, examenController.updateExamen);
router.delete('/:id', authorisation, examenController.deleteExamen);

module.exports = router;
