const router = require('express').Router();
const authController = require('../controllers/auth.controller');
const userController = require('../controllers/user.controller');
const { authorisation } = require('../middleware/auth.middleware');

//auth
router.post('/register', authController.signUp);
router.post('/login', authController.signIn);
router.get('/logout', authController.logOut);
router.get('/okForYou', authController.okForYou);

//user
router.get('/', authorisation, userController.getAllUsers);
router.get('/etudiant', userController.getAllEtudiantUser);
router.get('/admin', userController.getAllAdminUser);
router.get('/:id', userController.userInfo);
router.put('/:id', authorisation, userController.updateUser);
router.put('/etudiant/:id', authorisation, userController.updateUserEtudiant);
router.delete('/:id', authorisation, userController.deleteUser);
router.get(
  '/etudiant/search/:niveau/:parcours',
  userController.getEtudiantByNiveau
);
router.get('/matricule/:matricule', userController.getByMatricule);

module.exports = router;
