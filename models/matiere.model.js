const mongoose = require('mongoose')
const matiereSchema = new mongoose.Schema(
    {
     libelle:{
         type: String,
         required: true,
         minlenght: 2,
         maxlenght: 20,
         unique: true,
         trim: true
     },
     coefficient: {
         type: String,
         required: true,
         minLenght: 1,
         max: 2,
         
     },
     parcours:{
         type: String,
         required: true,
     },
     niveau : {
         type: String,
         required: true,
         trim: true,
     }
    },
    {
     timestamps: true
    }
)

const MatiereModel = mongoose.model('matiere', matiereSchema)

module.exports = MatiereModel;