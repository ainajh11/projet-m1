const mongoose = require('mongoose');
const Schema = mongoose.Schema;
ObjectId = Schema.ObjectId;
const examenSchema = new mongoose.Schema(
  {
    note: {
      type: Number,
      required: true,
    },
    matiere: { type: ObjectId, ref: 'matiere' },
    etudiant: { type: ObjectId, ref: 'user' },
    semestre: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const ExamenModel = mongoose.model('examen', examenSchema);

module.exports = ExamenModel;
