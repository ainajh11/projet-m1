const mongoose = require('mongoose');
const etudiantSchema = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
      minlenght: 4,
      maxlenght: 55,
      unique: true,
    },
    numero: {
      type: Number,
      required: true,
      minlenght: 1,
      maxlenght: 3,
    },
    parcours: {
      type: String,
      required: true,
    },
    matricule: {
      type: String,
      required: true,
    },
    niveau: {
      type: String,
      required: true,
    },
    dateNaiss: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const EtudiantModel = mongoose.model('etudiant', etudiantSchema);

module.exports = EtudiantModel;
