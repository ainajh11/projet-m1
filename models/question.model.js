const mongoose = require('mongoose');
const Schema = mongoose.Schema;
ObjectId = Schema.ObjectId;
const questionSchema = new mongoose.Schema(
  {
    question: {
      type: String,
      required: true,
      unique: true,
    },
    semestre: {
      type: String,
      required: true,
    },
    time: {
      type: Number,
      required: true,
    },
    all_reponse: {
      type: [String],
    },
    true_reponse: {
      type: [String],
      required: true,
    },
    identifiant: {
      type: String,
      required: true,
    },
    matiere: ObjectId,
  },
  {
    timestamps: true,
  }
);

const QuestionModel = mongoose.model('question', questionSchema);

module.exports = QuestionModel;
