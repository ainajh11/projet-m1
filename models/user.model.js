const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const userSchema = new mongoose.Schema(
  {
    pseudo: {
      type: String,
      required: true,
      minLength: 4,
      maxLength: 55,
      unique: true,
      trim: true,
    },

    nom: {
      type: String,
      required: true,
      minlenght: 4,
      maxlenght: 55,
      unique: true,
    },
    numero: {
      type: Number,
      required: true,
      minlenght: 1,
      maxlenght: 3,
    },
    parcours: {
      type: String,
      required: true,
    },
    niveau: {
      type: String,
      required: true,
    },
    dateNaiss: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
      max: 1024,
      minLength: 5,
    },
    role: {
      type: String,
      required: true,
    },
    matricule: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

//avant de sauvegarder dans la base
userSchema.pre('save', async function (next) {
  const salt = await bcrypt.genSalt();
  this.password = await bcrypt.hash(this.password, salt);
  next();
});

userSchema.statics.login = async function (pseudo, password) {
  const user = await this.findOne({ pseudo });
  if (user) {
    const auth = await bcrypt.compare(password, user.password);
    if (auth) {
      return user;
    }
    throw Error('Mot de passe inconnue');
  }
  throw Error('Pseudo Inconnu');
};

const UserModel = mongoose.model('user', userSchema);

module.exports = UserModel;
